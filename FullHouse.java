public class FullHouse extends Hand {
    public FullHouse(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    @Override
    public Card getTopCard() {
        if (getCard(1).getRank() == getCard(2).getRank()) {
            return getCard(2);
        }
        return getCard(4);
    }

    @Override
    boolean isValid() {
        if (size() != 5) return false;
        if (getCard(1).getRank() != getCard(2).getRank()) return false;
        if (getCard(1).getRank() != getCard(2).getRank() && getCard(2).getRank() != getCard(3).getRank()) return false;
        return getCard(3).getRank() == getCard(4).getRank();
    }

    @Override
    String getType() {
        return "FullHouse";
    }
}
