public class Triple extends Hand {
    public Triple(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    @Override
    boolean isValid() {
        if (size() != 3) return false;
        return getCard(0).equals(getCard(1)) && getCard(1).equals(getCard(2));
    }

    @Override
    String getType() {
        return "Triple";
    }
}
