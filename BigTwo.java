import java.util.ArrayList;

public class BigTwo implements CardGame {

    private int numOfPlayers;
    private Deck deck;
    private ArrayList<CardGamePlayer> playerList;
    private ArrayList<Hand> handsOnTable;
    private int currentPlayerIdx;
    private BigTwoUI ui;

    public BigTwo() {
        // (i) create 4 players and add them to the player list
        playerList = new ArrayList<>();
        numOfPlayers = 4;
        for (int i = 0; i < numOfPlayers; i++) {
            playerList.add(new CardGamePlayer());
        }

        // (ii) create a BigTwoUI object for providing the user interface
        handsOnTable = new ArrayList<>(); // initialize handsOnTable before passing object to BigTwoUi
        ui = new BigTwoUI(this);
    }

    @Override
    public int getNumOfPlayers() {
        return numOfPlayers;
    }

    @Override
    public Deck getDeck() {
        return deck;
    }

    @Override
    public ArrayList<CardGamePlayer> getPlayerList() {
        return playerList;
    }

    @Override
    public ArrayList<Hand> getHandsOnTable() {
        return handsOnTable;
    }

    @Override
    public int getCurrentPlayerIdx() {
        return currentPlayerIdx;
    }

    @Override
    public void start(Deck deck) {
        this.deck = deck;

        // (i) remove all the cards from the players as well as from the table
        for (int i = 0; i < numOfPlayers; i++) {
            playerList.get(i).removeAllCards();
        }
        handsOnTable.clear();
        // (ii) distribute the cards to the player
        for (int i = 0; i < deck.size(); i++) {
            playerList.get(i % numOfPlayers).addCard(deck.getCard(i));
        }
        for (CardGamePlayer player : playerList) {
            player.sortCardsInHand();
        }
        // (iii) identify the player who holds the Three of Diamonds
        Card diamond3 = new BigTwoCard(0, 2);
        int bigThreePlayerIdx = -1;
        for (int i = 0; i < numOfPlayers; i++) {
            CardGamePlayer currentPlayer = playerList.get(i);
            if (currentPlayer.getCardsInHand().contains(diamond3)) {
                bigThreePlayerIdx = i;
            }
        }

        // (iv) set both the currentPlayerIdx of the BigTwo object and the activePlayer of the BigTwoUI object to the index of the player who holds the Three of Diamonds
        currentPlayerIdx = bigThreePlayerIdx;
        ui.setActivePlayer(currentPlayerIdx);

        while (!endOfGame()) {
            // (v) call the repaint() method of the BigTwoUI object to show the cards on the table
            ui.repaint();

            // (vi) call the promptActivePlayer() method of the BigTwoUI object to prompt user to select cards and make his/her move
            int lastPlayerIndex = currentPlayerIdx;
            while (lastPlayerIndex == currentPlayerIdx)
                ui.promptActivePlayer();
            ui.setActivePlayer(currentPlayerIdx);
        }

        for (int i = 0; i < numOfPlayers; i++) {
            CardGamePlayer player = playerList.get(i);
            if (player.getNumOfCards() == 0) {
                ui.printMsg(String.format("%s wins the game.\n", player.getName()));
            } else {
                ui.printMsg(String.format("%s has %d cards in hand.\n", player.getName(), player.getNumOfCards()));
            }
        }
    }

    @Override
    public void makeMove(int playerIdx, int[] cardIdx) {
        checkMove(playerIdx, cardIdx);
        CardGamePlayer player = playerList.get(playerIdx);
        CardList cardList = new CardList();
        if (cardIdx != null) {
            for (int selectedCardIdx : cardIdx) {
                cardList.addCard(player.getCardsInHand().getCard(selectedCardIdx));
            }
        }
        if (cardList.isEmpty() && !handsOnTable.isEmpty()) {
            ui.printMsg("{Pass}\n");
            currentPlayerIdx++;
            currentPlayerIdx %= numOfPlayers;
            return;
        }
        Hand hand = composeHand(player, cardList);
        boolean isValidHand = false;
        if (hand != null) {
            if (handsOnTable.isEmpty()) {
                Card diamond3 = new BigTwoCard(0, 2);
                if (hand.contains(diamond3)) {
                   isValidHand = true;
                }
            } else {
                Hand lastHand = handsOnTable.get(handsOnTable.size() - 1);
                if (lastHand.size() == hand.size() && hand.beats(lastHand)) {
                    isValidHand = true;
                }
                if (lastHand.getPlayer().getName().equals(playerList.get(currentPlayerIdx).getName())) {
                    isValidHand = true;
                }
            }
        }
        if (isValidHand) {
            ui.printMsg(String.format("{%s} ", hand.getType()));
            hand.print();
            handsOnTable.add(hand);
            player.removeCards(cardList);
            currentPlayerIdx++;
            currentPlayerIdx %= numOfPlayers;
            return;
        }
        ui.printMsg("Not a legal move!!!\n");
    }

    @Override
    public void checkMove(int playerIdx, int[] cardIdx) {

    }

    @Override
    public boolean endOfGame() {
        for (int i = 0; i < numOfPlayers; i++) {
            if (playerList.get(i).getNumOfCards() == 0) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        // (i) create a Big Two card game
        BigTwo bigTwoGame = new BigTwo();

        // (ii) create and shuffle a deck of cards
        BigTwoDeck cardDeck = new BigTwoDeck();
        cardDeck.shuffle();

        // (iii) start the game with the deck of cards
        bigTwoGame.start(cardDeck);
    }

    public static Hand composeHand(CardGamePlayer player, CardList cards) {
        Hand[] possibleHands = {
                new Single(player, cards),
                new Pair(player, cards),
                new Triple(player, cards),
                new StraightFlush(player, cards),
                new Quad(player, cards),
                new FullHouse(player, cards),
                new Flush(player, cards),
                new Straight(player, cards)
        };

        for (Hand hand : possibleHands) {
            if (hand.isValid()) {
                return hand;
            }
        }

        // no valid hand can be composed from the specified list of cards
        return null;
    }
}
