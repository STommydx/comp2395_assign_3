import java.util.ArrayList;
import java.util.Arrays;

public abstract class Hand extends CardList {
    private CardGamePlayer player;

    public CardGamePlayer getPlayer() {
        return player;
    }

    public Card getTopCard() {
        return getCard(size() - 1);
    }

    public boolean beats(Hand hand) {
        if (getType().equals(hand.getType())) {
            return getTopCard().compareTo(hand.getTopCard()) > 0;
        }
        final ArrayList<String> levels = new ArrayList<>(Arrays.asList("Straight", "Flush", "FullHouse", "Quad", "StraightFlush"));
        int myLevel = levels.indexOf(getType());
        int hisLevel = levels.indexOf(hand.getType());
        return myLevel > hisLevel;
    }

    abstract boolean isValid();
    abstract String getType();

    public Hand(CardGamePlayer player, CardList cards) {
        this.player = player;
        for (int i = 0; i < cards.size(); i++) {
            addCard(cards.getCard(i));
        }
    }
}
