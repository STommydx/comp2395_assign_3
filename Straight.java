public class Straight extends Hand {
    public Straight(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    @Override
    boolean isValid() {
        if (size() != 5) return false;
        for (int i = 0; i < 4; i++) {
            int rankI = getCard(i).getRank();
            int rankIPlusOne = getCard(i + 1).getRank();
            if ((rankI + 1) % 13 != rankIPlusOne) return false;
        }
        return true;
    }

    @Override
    String getType() {
        return "Straight";
    }
}
