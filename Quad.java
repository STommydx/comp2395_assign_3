public class Quad extends Hand {
    public Quad(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    @Override
    public Card getTopCard() {
        if (getCard(0).getRank() == getCard(1).getRank()) {
            return getCard(3);
        }
        return getCard(4);
    }

    @Override
    boolean isValid() {
        if (size() != 5) return false;
        if (getCard(1).getRank() != getCard(2).getRank()) return false;
        if (getCard(2).getRank() != getCard(3).getRank()) return false;
        return getCard(0).getRank() == getCard(1).getRank() || getCard(3).getRank() == getCard(4).getRank();
    }

    @Override
    String getType() {
        return "Quad";
    }
}
