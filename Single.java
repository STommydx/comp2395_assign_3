public class Single extends Hand {
    public Single(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    @Override
    boolean isValid() {
        return size() == 1;
    }

    @Override
    String getType() {
        return "Single";
    }
}
