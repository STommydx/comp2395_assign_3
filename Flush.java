public class Flush extends Hand {
    public Flush(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    @Override
    boolean isValid() {
        if (size() != 5) return false;
        for (int i = 0; i < 4; i++) {
            if (getCard(i).getSuit() != getCard(i + 1).getSuit()) {
                return false;
            }
        }
        return true;
    }

    @Override
    String getType() {
        return "Flush";
    }
}
