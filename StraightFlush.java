public class StraightFlush extends Flush {
    public StraightFlush(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    @Override
    boolean isValid() {
        if (!super.isValid()) return false;
        for (int i = 0; i < 4; i++) {
            int rankI = getCard(i).getRank();
            int rankIPlusOne = getCard(i + 1).getRank();
            if ((rankI + 1) % 13 != rankIPlusOne) return false;
        }
        return true;
    }

    @Override
    String getType() {
        return "StraightFlush";
    }
}
