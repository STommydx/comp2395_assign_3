public class Pair extends Hand {
    public Pair(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    @Override
    boolean isValid() {
        if (size() != 2) return false;
        return getCard(0).getRank() == getCard(1).getRank();
    }

    @Override
    String getType() {
        return "Pair";
    }
}
